"""Fonctionnalités communes aux scripts utilisant LDAP"""
from ldap3 import Connection

SERVEUR_IMO = 'ldaps1.imo.universite-paris-saclay.fr'
SUFFIXE_IMO = 'dc=lmo,dc=fr'
OU_GROUPES_IMO = 'ou=Group,' + SUFFIXE_IMO
OU_COMPTES_IMO = 'ou=People,' + SUFFIXE_IMO
OU_AUTO_HOME_IMO = 'ou=auto.home,ou=autofs,' + SUFFIXE_IMO
ADMIN_IMO = 'cn=admin,dc=lmo,dc=fr'

SERVEUR_ADONIS = 'ldap.universite-paris-saclay.fr'
OU_COMPTES_ADONIS = 'ou=lmo,ou=hosts,dc=universite-paris-saclay,dc=fr'
ADMIN_ADONIS = 'cn=hosts,ou=admin,dc=universite-paris-saclay,dc=fr'


def SID_IMO(imo: Connection) -> str:
    """Retourne le SID Samba de l'IMO"""
    imo.search(
        SUFFIXE_IMO,
        '(objectClass=sambaDomain)',
        attributes=['sambaSID']
    )
    return imo.entries[0].sambaSID.value
