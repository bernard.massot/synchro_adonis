"""Manipulation des annuaires LDAP de l'IMO et d'Adonis"""
from os import chown, mkdir
import os.path
from glob import glob
from shutil import copy
from typing import Any, Dict, List, Optional

from ldap3 import (MODIFY_REPLACE, # paquet python3-ldap3
                   Server, Tls, Connection, HASHED_SALTED_SHA)
from ldap3.abstract.entry import Entry
from ldap3.utils.hashed import hashed
from unidecode import unidecode # paquet python3-unidecode
from FsQuota import Quota # paquet python3-fsquota
from gitlab.client import Gitlab # paquet python3-gitlab
from Cryptodome.Hash import MD4 # paquet python3-pycryptodome

from .mots_de_passe import ADONIS_PASSWD, IMO_PASSWD, TOKEN_GITLAB
from .utilitaires_ldap import (SERVEUR_ADONIS, OU_COMPTES_ADONIS, ADMIN_ADONIS,
                              SERVEUR_IMO, OU_COMPTES_IMO, OU_AUTO_HOME_IMO,
                              ADMIN_IMO, SID_IMO)
from api_adonis.api_adonis import Adonis, GroupeInconnu, SessionAdonis


# {sigle_Adonis: gid_IMO, …}
GROUPES_IMO = {
    'Bibliothèque':      400, # Bibliothèque
    'ANH':               300, # Analyse harmonique
    'AGA':               900, # AGA
    'ANEDP':             200, # ANEDP
    'PS':                700, # Probastat
    'TOPO':              800, # Topodyn
    'Pôle gestion':      600, # Pôle gestion
    'Pôle informatique': 600, # Pôle informatique
    'Pôle logistique':   600, # Pôle logistique
    'Pôle enseignement': 600, # Pôle enseignement
    'FMJH':              600, # FMJH
}
GROUPE_PAR_DEFAUT = 600 # groupe "mat"
# {gid: chemin, …}
BASES_HOMES_IMO = {
    400: '/users/bib/', # Bibliothèque
    300: '/users/anh/', # Analyse harmonique
    900: '/users/aga/', # AGA
    200: '/users/anedp/', # ANEDP
    700: '/users/probastat/', # Probastat
    800: '/users/topodyn/', # Topodyn
    600: '/users/mat/', # Gestion, Informatique, Logistique, Enseignement
    500: '/users/ext/', # Extérieurs
}
SERVEUR_GITLAB = 'https://gitlab.imo.universite-paris-saclay.fr/'


class CompteLDAP():
    """Compte d'un annuaire LDAP"""
    def __init__(self, entry: Entry):
        self.entry = entry

    def __str__(self):
        return self.entry.uid.value


class CompteAdonis(CompteLDAP):
    """Compte de l'annuaire Adonis"""
    pass


class AnnuaireAdonis():
    """Annuaire des comptes LDAP de la branche IMO d'Adonis"""
    def __init__(self):
        self.ldap = Connection(Server(SERVEUR_ADONIS, use_ssl=True,
                                      tls=Tls(ciphers='DEFAULT')),
                               user=ADMIN_ADONIS,
                               password=ADONIS_PASSWD,
                               auto_bind=True)
        self.comptes = {e.uid.value: CompteAdonis(e) for e in self._comptes_ldap()}
        self.session_api = SessionAdonis()

    def _comptes_ldap(self) -> List[Entry]:
        self.ldap.search(
            OU_COMPTES_ADONIS,
            '(objectClass=posixAccount)',
            attributes=['sn', 'givenName', 'uid',
                        'userPassword', 'sambaLMPassword',
                        'sambaNTPassword', 'sambaPwdLastSet',
                        'supannAliasLogin']
        )
        return self.ldap.entries


class CompteIMO(CompteLDAP):
    """Compte de l'annuaire LDAP de l'IMO"""
    SERVEUR_NFS = 'c3po.math.u-psud.fr'
    # Les quotas sont en kB
    QUOTA_SOFT = int(40e6)
    QUOTA_HARD = int(50e6)
    FICHIER_LOGIN_COURT = '.login-court-adonis.txt'


    def __ne__(self, compte: CompteAdonis) -> bool:
        """Comparaison entre un compte IMO et un compte Adonis"""
        # On ne regarde que l'attribut userPassword
        if isinstance(compte, CompteAdonis):
            return self.entry.userPassword != compte.entry.userPassword
        else:
            return NotImplemented

    def mise_a_jour(self, imo: 'AnnuaireIMO', compte: CompteAdonis):
        """Mise à jour depuis un compte Adonis"""
        attributs = ['userPassword', 'sambaLMPassword', 'sambaNTPassword',
                     'sambaPwdLastSet']
        imo.ldap.modify(self.entry.entry_dn, {
            attribut: [(MODIFY_REPLACE, [compte.entry[attribut].value])]
            for attribut in attributs
        })

    @classmethod
    def creation_utilisateur_ldap(cls, imo: 'AnnuaireIMO',
                                  attributs: Dict[str, Any]):
        """Création d'un utilisateur dans le LDAP de l'IMO"""
        # Le paramètre "attributs" contient les champs et valeurs des attributs
        # LDAP de l'utilisateur à créer.
        uid = attributs['uid']
        gidNumber = attributs['gidNumber']
        uidNumber = imo.max_uidnumber() + 1
        if 'supannAliasLogin' in attributs:
            login_court = attributs['supannAliasLogin']
            del attributs['supannAliasLogin']
        else:
            login_court = None

        # Création du posixAccount
        cn = attributs['givenName'] + ' ' + attributs['sn']
        classes = ['top' , 'posixAccount', 'shadowAccount', 'person',
                   'inetOrgPerson', 'organizationalPerson']
        if 'sambaNTPassword' in attributs:
            classes.append('sambaSamAccount')
            attributs['sambaSID'] = f'{imo.SID}-{uidNumber}'
            attributs['sambaAcctFlags'] = '[U          ]'
        imo.ldap.add(
            dn=f'uid={uid},{OU_COMPTES_IMO}',
            object_class=classes,
            attributes={
                **attributs,
                'uidNumber' : uidNumber,
                'cn': cn,
                'gecos': unidecode(cn),
                'loginShell': '/bin/bash',
                'uidNumber': uidNumber,
                'homeDirectory': f'/home/{uid}',
            }
        )

        # Création de l'automount
        base_home = BASES_HOMES_IMO[gidNumber]
        home = base_home + uid
        imo.ldap.add(
            dn=f'cn={uid},{OU_AUTO_HOME_IMO}',
            object_class=['top', 'automount'],
            attributes={
                'automountInformation': '-fstype=nfs,hard,intr,nodev,nosuid '
                                        f'{cls.SERVEUR_NFS}:{home}'
            }
        )

        # Création du répertoire home
        cls._creation_home(base_home, home, uidNumber, gidNumber, login_court)

        cls._creation_compte_gitlab(username=uid, name=cn, email=attributs['mail'])

    @classmethod
    def creation_depuis_adonis(cls, imo: 'AnnuaireIMO', compte: CompteAdonis,
                               adonis: Adonis):
        """Création d'un compte IMO depuis un compte Adonis"""
        attributs_adonis = [
            'givenName', 'sn', 'userPassword', 'uid', 'sambaLMPassword',
            'sambaNTPassword', 'sambaPwdLastSet', 'supannAliasLogin']
        attributs = {a: compte.entry[a].value for a in attributs_adonis}
        try:
            sigle_groupe = adonis.sigle_groupe_compte(compte.entry.uid.value)
            attributs['gidNumber'] = GROUPES_IMO[sigle_groupe]
        except GroupeInconnu:
            attributs['gidNumber'] = GROUPE_PAR_DEFAUT
        attributs['mail'] = f'{compte.entry.uid}@universite-paris-saclay.fr'

        cls.creation_utilisateur_ldap(imo, attributs)

    @classmethod
    def _creation_home(cls, base_home: str, home: str,
                       uidNumber: int, gidNumber: int,
                       login_court: Optional[str]):
        """Création du répertoire home et de ce qui va avec"""
        # Répertoire home
        mkdir(home, mode=0o700)
        chown(home, uidNumber, gidNumber)
        for fichier in glob('/etc/skel/.bash*'):
            copy(fichier, home)
            chown(os.path.join(home, os.path.basename(fichier)),
                  uidNumber, gidNumber)
        if login_court:
            fichier_login_court = os.path.join(home, cls.FICHIER_LOGIN_COURT)
            with open(fichier_login_court, 'wt') as f:
                f.write(login_court + '\n')
            chown(fichier_login_court, uidNumber, gidNumber)

        # Quota
        quota = Quota(base_home)
        quota.setqlim(uidNumber, bsoft=cls.QUOTA_SOFT, bhard=cls.QUOTA_HARD,
                      isoft=0, ihard=0)
        quota.sync() # utile ?

    @staticmethod
    def _creation_compte_gitlab(username: str, name: str, email: str):
        """Création du compte GitLab"""
        gitlab = Gitlab(SERVEUR_GITLAB, private_token=TOKEN_GITLAB)
        gitlab.users.create(
            username=username,
            force_random_password=True,
            name=name,
            email=email,
            provider='ldapmain',
            extern_uid=f'uid={username},{OU_COMPTES_IMO}',
            skip_confirmation=True,
        )

    @staticmethod
    def _sambaNTPassword(mot_de_passe: str) -> str:
        """Retourne un mot de passe au format NT Password."""
        return MD4.new(mot_de_passe.encode('utf-16le')).digest().hex().upper()

    def modification_mot_de_passe(self, imo: 'AnnuaireIMO', mot_de_passe: str):
        """Modifie le mot de passe Unix et Samba."""
        attributs = {'userPassword': hashed(HASHED_SALTED_SHA, mot_de_passe)}
        if 'sambaNTPassword' in self.entry.entry_attributes:
            attributs['sambaNTPassword'] = self._sambaNTPassword(mot_de_passe)
        imo.ldap.modify(self.entry.entry_dn, {
                        attribut: [(MODIFY_REPLACE, [valeur])]
                        for attribut, valeur in attributs.items()
        })


class AnnuaireIMO():
    """Annuaire des comptes LDAP de l'IMO"""
    def __init__(self):
        self.ldap = Connection(Server(SERVEUR_IMO, use_ssl=True),
                                      user=ADMIN_IMO, password=IMO_PASSWD,
                                      auto_bind=True)
        self.comptes = {e.uid.value: CompteIMO(e) for e in self._comptes_ldap()}
        self.SID = SID_IMO(self.ldap)

    def _comptes_ldap(self) -> List[Entry]:
        self.ldap.search(
            OU_COMPTES_IMO,
            '(objectClass=posixAccount)',
            attributes=['uid', 'uidNumber', 'userPassword', 'sambaNTPassword']
        )
        return self.ldap.entries

    def max_uidnumber(self) -> int:
        """Retourne le plus grand uidNumber courant de l'IMO"""
        self.ldap.search(
            OU_COMPTES_IMO,
            '(uid=*)',
            attributes=['uidNumber']
        )
        return max(e.uidNumber.value for e in self.ldap.entries)
