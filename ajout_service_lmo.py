#!/usr/bin/python3
"""Ajout du service "lmo" aux comptes Adonis de l'IMO"""
from api_adonis.api_adonis import (SessionAdonis, ID_GROUPE_DMO,
                                   ID_GROUPE_FMJH, id_service, ServiceInconnu)


# Groupes dont les membres doivent avoir le service "lmo"
GROUPES_IMO = [
    94,   # Bibliothèque Jacques Hadamard
    941,  # Analyse Harmonique
    942,  # Arithmétique et Géométrie Algébrique
    943,  # Analyse Numérique et Equations aux Dérivées Partielles
    944,  # Probabilités, Statistique et Modélisation
    945,  # Topologie et Dynamique
    9392, # Département d’Enseignement
    9395, # Pôle Gestion
    9396, # Pôle informatique
    9397, # Pôle Logistique
    9398, # Pôle Enseignement
]
NOM_SERVICE_LMO = 'lmo'
ID_HISTORIQUE_SERVICE_LMO = 61


def ajout_service_lmo(session: SessionAdonis):
    """Ajout du service lmo aux comptes Adonis qui ne l'ont pas"""
    try:
        serviceId = id_service(adonis, NOM_SERVICE_LMO)
    except ServiceInconnu:
        # Au cas où le service change de nom, mais pas de numéro
        serviceId = ID_HISTORIQUE_SERVICE_LMO
    reponse = session.get(f'/groups/{ID_GROUPE_DMO}/children')
    groupes = reponse.json()
    reponse = session.get(f'/groups/{ID_GROUPE_FMJH}/children')
    groupes.extend(reponse.json())
    for groupe in groupes:
        groupeId = groupe['groupeId']
        # La FMJH n'est pas dans l'arborescence de l'IMO
        if groupeId in GROUPES_IMO + [ID_GROUPE_FMJH]:
            reponse = session.get(f'/groups/{groupeId}/members')
            if reponse.text:
                membres = reponse.json()
                for membre in membres:
                    compteId = membre['compteId']
                    reponse = session.get(f'/accounts/{compteId}/services')
                    compte = reponse.json()
                    if 'lmo' not in [s['service'] for s in compte['services']]:
                        reponse = session.post(f'/accounts/{compteId}/services/{serviceId}')
            

if __name__ == '__main__':
    adonis = SessionAdonis()
    ajout_service_lmo(adonis)
