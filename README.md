Ce dépôt comporte un module de manipulation de l'[API REST d'Adonis](https://adonis-api.universite-paris-saclay.fr/), un module de manipulation d'annuaires LDAP, et quelques utilitaires.

Ces programmes ont été conçus pour le Laboratoire de Mathématiques d'Orsay (LMO), mais ceux qui manipulent l'API REST devraient être facilement adaptables à un autre laboratoire. C'est nettement moins vrai pour le côté LDAP.

[[_TOC_]]

# API REST

## Configuration

### Authentification

L'utilisation de l'API REST nécessite l'utilisation d'un compte à faire créer par la DSI de Paris-Saclay (dont le login est, a priori, du type `ws.<sigle_du_labo>`).

Ces identifiants sont à mettre dans un fichier `api_adonis/identifiants_api_adonis.py`, dans des variables `USERNAME` et `PASSWORD`.

### Variables

Certaines valeurs sont à modifier dans des constantes au début de [api\_adonis/api\_adonis.py](api_adonis/api_adonis.py).

La variable `SERVEUR` contient l'adresse de l'API REST de la DSI.

Dans le cas du LMO, son service informatique gère les comptes Adonis affectés :
- aux sous-groupes du groupe _Département de Mathématiques_, dont l'identifiant est stocké dans `ID_GROUPE_LABO` ;
- à l'organisme _FMJH_, dont l'identifiant est stocké dans `ID_GROUPE_FMJH`.

Si votre labo est architecturé différemment, vous aurez à modifier l'affectation de la variable `_groupes` dans la fonction `Adonis._initialisation_nomenclature()`.

Bien qu'on pourrait, théoriquement, récupérer les identifiants des différentes entités d'Adonis à chaque fois qu'on en a besoin, ce module Python enregistre, pour des raisons de performance, tous ces identifiants de nomenclature une seule fois, au début de chaque session. Pour ce faire, il faut préciser les entités qu'on envisage d'utiliser, en les renseignant dans des constantes.\
Notez que, pour une exécution plus rapide, il existe une fonction `Adonis._initialisation_nomenclature_depuis_cache()`, qui remplit, avec des valeurs en dur, les dictionnaires de la nomenclature d'Adonis.

La variable `NOMENCLATURE` contient les clés suivantes :
- `groupes` : les sigles des groupes Adonis à utiliser ;
- `categories` : les catégories des statuts à utiliser ;
- `fonctions` : les fonctions des membres du labo ;
- `organismes` : les tutelles des membres du labo ;
- `bâtiments` : un dictionnaire de campus, contenant chacun une liste des bâtiments du labo ;
- `services` a deux entrées :
  - `labo` : le nom du service Adonis du labo, permettant d'avoir une branche labo dans LDAP ;
  - `impression` : le nom du service Adonis d'impression, utile si vous utilisez Canon Uniflow.

Les variables `CAMPUS_PAR_DEFAUT` et `BATIMENT_PAR_DEFAUT` ont des noms qui parlent d'eux-mêmes.

Faute de pouvoir récupérer la valeur via l'API (pour l'instant), la variable `TYPE_EMAIL_PRINCIPAL` contient l'identifiant du type d'email principal, utilisé pour retourner l'adresse d'un compte Adonis.

## Utilisation

Une fois le fichier `api_adonis/identifiants_api_adonis.py` créé, le répertoire `api_adonis` est utilisable comme module Python.

L'accès à l'API se fait en instanciant la classe `Adonis` :
```python
from api_adonis.api_adonis import Adonis

adonis = Adonis()
```

Le constructeur peut prendre en argument l'URL du serveur. Ainsi, pour utiliser le serveur de test :
```python
adonis = Adonis('https://adonis-dev.dsi.universite-paris-saclay.fr/api/')
```

La création ou affectation de compte s'effectue en fournissant un dictionnaire au format suivant :
| Champ            | Type                        | Optionnel |
|------------------|-----------------------------|-----------|
| civilite         | `str` (`'M.'` ou `'Mme'`)   | non       |
| nom              | `str`                       | non       |
| prenom           | `str`                       | non       |
| date\_naissance  | `datetime.date`             | non       |
| groupe           | `str`                       | non       |
| categorie        | `str`                       | non       |
| fonction         | `str`                       | non       |
| organisme        | `str`                       | non       |
| date\_expiration | `datetime.date`             | oui       |
| bureau           | `str`                       | oui       |
| telephone        | `str`                       | oui       |
| campus           | `str`                       | oui       |
| batiment         | `str`                       | oui       |

Les champs `groupe`, `categorie`, `fonction`, `organisme`, `campus` et `batiment` doivent contenir une des valeurs présentes dans la variable `NOMENCLATURE`.

### Fonctions disponibles

Pour créer un compte, ou procéder à son affectation dans le bon groupe avec les bons paramètres, utilisez la fonction `creation_ou_affectation_compte()` :

```python
from datetime import date

adonis = Adonis()
attributs = {
    'civilite':         'M.',
    'nom':              'Massot',
    'prenom':           'Bernard',
    'date_naissance':   date(day=1, month=1, year=1970),
    'groupe':           'Pôle informatique',
    'categorie':        'Personnel co-tutélaire',
    'fonction':         'Informaticien',
    'organisme':        'CNRS',
    'bureau':           '2S1',
}
compte = adonis.creation_ou_affectation_compte(attributs)
```

Cette fonction retourne un dictionnaire contenant les informations du compte, au [format décrit dans l'API](https://adonis-api.universite-paris-saclay.fr/doc/index.html#tag/Comptes/operation/AccountsController::getAccount).

 

La fonction `mise_a_jour_compte()` met à jour un compte Adonis, et prend en paramètres le login, la catégorie du statut à mettre à jour, la date d'expiration du statut, ainsi que la fonction, le bureau, et le téléphone liés au statut concerné du compte :
```python
from datetime import date

adonis.mise_a_jour_compte(
    login='bernard.massot',
    categorie='Personnel co-tutélaire',
    date_expiration=date(day=1, month=1, year=2024),
    fonction='Informaticien',
    bureau='2S1',
    telephone='',
)
```
En pratique, cette fonction ne modifie – pour l'instant – que la date d'expiration.

 

La fonction `modification_mot_de_passe()` met à jour le mot de passe d'un compte :
```python
adonis.modification_mot_de_passe('bernard.massot', 'blabla')
```

 

La fonction `suppression_service()` enlève un service Adonis donné d'un compte :
```python
adonis.suppression_service('vpn', 'bernard.massot')
```

 

La fonction `email_compte()` retourne l'email principal d'un compte, et prend en paramètre un dictionnaire de compte, tel que celui retourné par `creation_ou_affectation_compte()` :
```python
compte = adonis.creation_ou_affectation_compte(attributs)
email = adonis.email_compte(compte)
```

 

La fonction `sigle_groupe_compte()` retourne le sigle du groupe Adonis d'un compte :
```python
groupe = adonis.sigle_groupe_compte('bernard.massot')
```

### Exceptions Python

Le module `api_adonis` définit les exceptions suivantes :
| Nom                           | Explication                                                                                                           |
|-------------------------------|-----------------------------------------------------------------------------------------------------------------------|
| `LoginInconnu`                | le login n'a pas été trouvé dans Adonis                                                                               |
| `CompteInconnu`               | le compte Adonis, cherché par Id, n'a pas été trouvé (en principe, `LoginInconnu` devrait être levée avant)           |
| `GroupeInconnu`               | lors de l'appel à `sigle_groupe_compte()`, le compte de login donné n'appartient à aucun groupe connu                 |
| `EchecCreationCompte`         | la requête de création de compte faite par `creation_ou_affectation_compte()` a échoué                                |
| `EchecModificationMotDePasse` | la requête de modification de mot de passe par `modification_mot_de_passe()` a échoué                                 |
| `CampusInconnu`               | un campus donné dans `NOMENCLATURE` n'a pas été trouvé par Adonis                                                     |
| `BatimentInconnu`             | un bâtiment donné dans `NOMENCLATURE` n'a pas été trouvé par Adonis                                                   |
| `ServiceInconnu`              |  un de services donnés dans `NOMENCLATURE`, ou celui passé en paramètre à `suppression_service()`, n'a pas été trouvé |

## Tests
Les tests sont faits pour [pytest](https://pytest.org/). Ils utilisent [requests-mock](https://github.com/jamielennox/requests-mock), et non le [serveur de test d'Adonis](https://adonis-dev.dsi.universite-paris-saclay.fr/api/), entre autres afin de ne pas dépendre de l'état de ce serveur.

# LDAP

Le programme [synchro\_ldap.py](synchro_ldap.py) permet de créer, et mettre à jour, de nouvelles entrées dans l'annuaire LDAP du LMO, en utilisant certaines valeurs extraites de l'annuaire LDAP d'Adonis. Il crée également des comptes sur le serveur GitLab du LMO.

## Fonctionnement des branches labo

En plus de sa branche LDAP générale, Adonis permet la création d'une « branche labo » spécifique pour un labo donné, qui contiendra notamment les mots de passe des utilisateurs, en version cryptée. Ces champs de mots de passe sont directement réutilisables dans un autre annuaire LDAP classique.

La création de cette branche labo marche de paire avec la création d'un nouveau service Adonis, nommé d'après le sigle du labo. Seuls les comptes Adonis ayant ce service activé apparaissent dans la branche labo.

## Configuration

L'accès à la branche labo se fait via des identifiants qui sont fournis, sur demande, par la DSI de Paris-Saclay.

Les logins d'accès à la branche labo et au LDAP du LMO sont dans le fichier [annuaires\_ldap/utilitaires\_ldap.py](annuaires_ldap/utilitaires_ldap.py), dans les variables `ADMIN_ADONIS` et `ADMIN_IMO`.

Les mots de passe d'accès à la branche labo et au LDAP du LMO, ainsi que le _token_ d'accès à l'API GitLab, sont à mettre dans un fichier `annuaires_ldap/mots_de_passe.py`, dans des variables `ADONIS_PASSWD`, `IMO_PASSWD`, et `TOKEN_GITLAB`.

## Utilisation

La synchronisation au fil de l'eau est effectuée par [synchro\_ldap.py](synchro_ldap.py).

Le script [creer\_utilisateur\_local.py](creer_utilisateur_local.py) permet de créer un compte LDAP du LMO non lié à Adonis.

Le script [suppression\_compte\_ldap.py](suppression_compte_ldap.py) supprime un compte LDAP du LMO.
