#!/usr/bin/python3
"""Création d'un utilisateur LDAP local (non synchronisé avec Adonis)"""
from os import system
from typing import Any, Dict, List, Tuple

from ldap3 import HASHED_SALTED_SHA # paquet python3-ldap3
from ldap3.utils.hashed import hashed
from dialog import Dialog # paquet python3-dialog
from unidecode import unidecode # paquet python3-unidecode
import cracklib # paquet python3-cracklib

from annuaires_ldap.annuaires import AnnuaireIMO, CompteIMO
from annuaires_ldap.utilitaires_ldap import OU_GROUPES_IMO


def liste_groupes(imo: AnnuaireIMO) -> Tuple[List[Tuple[str, str]], Dict[str, int]]:
    """Retourne les descriptions et gid des groupes de l'IMO"""
    groupes = ['anedp', 'anh', 'bib', 'ext', 'mat', 'probastat', 'topodyn',
               'geo']
    choices = [] # type: List[Tuple[str, str]]
    gids = {} # type: Dict[str, int]
    for groupe in groupes:
        imo.ldap.search(OU_GROUPES_IMO, f'(cn={groupe})',
                        attributes=['gidNumber', 'description'])
        entry = imo.ldap.entries[0]
        gids[groupe] = entry.gidNumber.value
        choices.append((groupe, entry.description.value))

    return choices, gids


def saisie(imo: AnnuaireIMO, gui: Dialog) -> Dict[str, Any]:
    """Saisie des champs de l'utilisateur"""
    champs = {} # type: Dict[str, str|int]
    gui.set_background_title("Création d'utilisateur LDAP local")

    # Nom et prénom
    nom = ''
    prenom = ''
    while not nom or not prenom:
        # Paramètres des champs :
        #   titre, ligne titre, colonne titre, valeur par défaut,
        #   ligne champ, colonne champ, largeur champ, longueur maximale champ
        code, reponse = gui.form('Civilité (avec accents)', [
            ['Nom',    1, 1, nom,    1, 10, 40, 100],
            ['Prénom', 2, 1, prenom, 2, 10, 40, 100]
        ])
        if code != Dialog.OK:
            exit()
        nom, prenom = reponse
        if not nom or not prenom:
            gui.msgbox('Il faut saisir un nom et un prénom.')
    champs['nom'] = nom
    champs['prenom'] = prenom

    # Login
    login_par_defaut = unidecode(f'{prenom}.{nom}').lower().replace(' ', '-')
    code, reponse = gui.inputbox('Login', init=login_par_defaut)
    if code != Dialog.OK:
        exit()
    assert type(reponse) is str
    champs['login'] = reponse

    # Groupe
    choices, gids = liste_groupes(imo)
    code, reponse = gui.menu('Groupe', choices=choices)
    if code != Dialog.OK:
        exit()
    assert type(reponse) is str
    champs['gidNumber'] = gids[reponse]

    # Courriel
    mail_par_defaut = champs['login'] + '@universite-paris-saclay.fr'
    code, reponse = gui.inputbox('Adresse e-mail', width=50, init=mail_par_defaut)
    if code != Dialog.OK:
        exit()
    assert type(reponse) is str
    champs['mail'] = reponse

    # Mot de passe
    mot_de_passe1 = ''
    mot_de_passe2 = ''
    trop_simple = True
    while mot_de_passe1 != mot_de_passe2 or mot_de_passe1 == '':
        while trop_simple:
            code, mot_de_passe1 = gui.passwordbox('Mot de passe', insecure=True)
            if code != Dialog.OK:
                exit()
            if cracklib.simple(mot_de_passe1):
                gui.msgbox('Mot de passe trop simple')
            else:
                trop_simple = False
        code, mot_de_passe2 = gui.passwordbox('Mot de passe à nouveau', insecure=True)
        if code != Dialog.OK:
            exit()
        if mot_de_passe1 != mot_de_passe2:
            gui.msgbox('Les mots de passe saisis sont différents.')
            trop_simple = True
    assert type(mot_de_passe1) is str
    champs['mots_de_passe'] = mot_de_passe1

    return champs


def creation_utilisateur(imo: AnnuaireIMO, prenom: str, nom: str, login: str,
                         mots_de_passe: str, gidNumber: int, mail: str=None):
    """Création d'un utilisateur LDAP local"""
    attributs = {
        'givenName': prenom,
        'sn': nom,
        'uid': login,
        'userPassword': hashed(HASHED_SALTED_SHA, mots_de_passe),
        'gidNumber': gidNumber,
    }
    if mail:
        attributs['mail'] = mail
    CompteIMO.creation_utilisateur_ldap(imo, attributs)


if __name__ == '__main__':
    imo = AnnuaireIMO()
    gui = Dialog()
    champs = saisie(imo, gui)
    creation_utilisateur(imo, **champs)
    gui.msgbox(f'Utilisateur {champs["login"]} créé avec succès.')
    system('clear')
