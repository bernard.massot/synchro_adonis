#!/usr/bin/python3
"""Synchronisation de l'annuaire LDAP de l'IMO avec celui de Paris-Saclay"""
from smtplib import SMTP
from email.message import EmailMessage

from gitlab.exceptions import GitlabCreateError

from annuaires_ldap.annuaires import AnnuaireAdonis, CompteAdonis, AnnuaireIMO, CompteIMO
from api_adonis.api_adonis import Adonis


def envoi_mail_creation(compte: CompteAdonis):
    """Envoi d'un mail au svp pour signaler la création d'un compte"""
    SMTP_UPSAY = 'smtp.universite-paris-saclay.fr'
    EXPEDITEUR = 'noreply@universite-paris-saclay.fr'
    DESTINATAIRE = 'svp.math@universite-paris-saclay.fr'

    msg = EmailMessage()
    msg['From'] = EXPEDITEUR
    msg['To'] = DESTINATAIRE
    msg['Subject'] = f'compte LDAP créé : {compte.entry.uid}'
    msg.set_content(f"Le compte {compte.entry.uid} vient d'être créé par "
                     "la synchronisation automatique avec Adonis.")
    serveur = SMTP(SMTP_UPSAY)
    serveur.send_message(msg)
    serveur.quit()


def synchro():
    """Mise à jour du LDAP de l'IMO depuis Adonis"""
    adonis = AnnuaireAdonis()
    api_adonis = Adonis()
    imo = AnnuaireIMO()
    for login, compte in adonis.comptes.items():
        if login in imo.comptes:
            if imo.comptes[login] != compte:
                imo.comptes[login].mise_a_jour(imo, compte)
        else:
            try:
                CompteIMO.creation_depuis_adonis(imo, compte, api_adonis)
                envoi_mail_creation(compte)
            except GitlabCreateError as erreur:
                print('Problème de création de compte GitLab pour',
                      compte, ':', erreur)


if __name__ == '__main__':
    synchro()
