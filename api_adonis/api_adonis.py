"""Implémentation de l'API REST d'Adonis"""
import json
import random
from string import ascii_lowercase, ascii_letters, digits
from typing import Dict, List, Literal, Optional, TypedDict, Union
from datetime import date
import logging

from unidecode import unidecode # paquet python3-unidecode
import requests # paquet python3-requests
from requests import Session, Response
from requests.auth import AuthBase

from .identifiants_api_adonis import USERNAME, PASSWORD
from .types import JSONcompte, JSONstatut


SERVEUR = 'https://adonis-api.universite-paris-saclay.fr/v1'
ID_GROUPE_LABO = 126 # Département de Mathématiques
ID_GROUPE_FMJH = 6862 # Fondation Mathématique Jacques Hadamard

# Intitulés Adonis utilisés par le labo
NOMENCLATURE = {
    'groupes': [
        'Bibliothèque',
        'Pôle logistique',
        'Pôle informatique',
        'Pôle gestion',
        'Pôle enseignement',
        'TOPO',
        'PS',
        'AGA',
        'ANEDP',
        'ANH',
    ],
    'categories': [
        'Chercheur co-tutélaire',
        'Enseignant-Chercheur co-tutélaire',
        'Enseignant co-tutélaire',
        'Personnel co-tutélaire',
        'Personnel administratif ou technique co-tutélaire',
    ],
    'fonctions': [
        'Enseignant-Chercheur',
        'Chercheur',
        'Enseignant',
        'Ingénieur de Recherche',
        'Gestionnaire',
        'Informaticien',
        'Agent Technique', # pour le service logistique
        'Secrétaire pédagogique',
        'Bibliothécaire',
        'Doctorant',
        'Etudiant',
        'Ingénieur',
    ],
    'organismes': [
        'UPSa', # Université Paris-Saclay
        'CNRS',
        'INRIA',
        'IHES',
        'AOPublic', # Autre organisme public
        'AOPrive', # Autre organisme privé
    ],
    'bâtiments': {
        'Bures-Orsay-Gif': ['307'],
    },
    'services':  {
        'labo':       'lmo',
        'impression': 'impression',
    },
}
CAMPUS_PAR_DEFAUT = 'Bures-Orsay-Gif'
BATIMENT_PAR_DEFAUT = '307'
# En attendant de pouvoir obtenir les types d'emails par l'API REST
TYPE_EMAIL_PRINCIPAL = 0


logger = logging.getLogger('api_adonis')


class AdonisRestAuth(AuthBase):
    """Authentification API REST d'Adonis"""
    # cf. https://adonis-api.universite-paris-saclay.fr/v1/doc/index.html#tag/Authentification
    def __init__(self, username: str, password: str, serveur: str):
        self.serveur = serveur
        state = ''.join(random.choices(ascii_lowercase, k=32))
        self.data = {
            'username': username,
            'password': password,
            'state': state
        }

    def __call__(self, request):
        reponse = requests.post(self.serveur + '/auth/signin', data=self.data).json()
        request.headers['Authorization'] = 'Bearer ' + reponse['access_token']
        return request


class SessionAdonis(Session):
    """Session requests spécifiquement faite pour Adonis"""
    def __init__(self, serveur=SERVEUR):
        super().__init__()
        self.serveur = serveur
        self.auth = AdonisRestAuth(USERNAME, PASSWORD, serveur)
        self.headers['Content-Type'] = 'application/x-www-form-urlencoded'

    def get(self, url: str, *args, **kwargs) -> Response:
        """Envoie une requête GET avec l'URL de l'API Adonis préfixée."""
        return super().get(self.serveur + url, *args, **kwargs)

    def post(self, url: str, *args, **kwargs) -> Response:
        """Envoie une requête POST avec l'URL de l'API Adonis préfixée."""
        return super().post(self.serveur + url, *args, **kwargs)

    def delete(self, url: str, *args, **kwargs) -> Response:
        """Envoie une requête DELETE avec l'URL de l'API Adonis préfixée."""
        return super().delete(self.serveur + url, *args, **kwargs)

    def put(self, url: str, *args, **kwargs) -> Response:
        """Envoie une requête PUT avec l'URL de l'API Adonis préfixée."""
        return super().put(self.serveur + url, *args, **kwargs)


class LoginInconnu(Exception):
    def __init__(self, login: str):
        super().__init__(f'Login non trouvé dans Adonis : {login}')


class CompteInconnu(Exception):
    def __init__(self, compteId: int):
        super().__init__(f'Compte non trouvé dans Adonis : {compteId}')


class GroupeInconnu(Exception):
    def __init__(self, login: str):
        super().__init__(f"Compte affecté à aucun groupe du labo : {login}")


class EchecCreationCompte(Exception):
    def __init__(self, prenom: str, nom: str, message: str=''):
        erreur = f'Échec de création du compte Adonis pour {prenom} {nom}'
        if message:
            erreur += f' ("{message}")'
        super().__init__(erreur)


class EchecModificationMotDePasse(Exception):
    def __init__(self, login: str):
        super().__init__(f'Échec de modification du mot de passe du compte {login}')


class CampusInconnu(Exception):
    def __init__(self, campus: str):
        super().__init__(f'Campus non trouvé dans Adonis : {campus}')


class BatimentInconnu(Exception):
    def __init__(self, batiment: str):
        super().__init__(f'Bâtiment non trouvé dans Adonis : {batiment}')


class ServiceInconnu(Exception):
    def __init__(self, nom_service: str):
        super().__init__(f'Service non trouvé dans Adonis : {nom_service}')


def id_service(session: SessionAdonis, nom_service: str):
    """Retourne le serviceId pour un nom de service donné."""
    for service in session.get('/services').json():
        if service['service'] == nom_service:
            return service['serviceId']
    else:
        raise ServiceInconnu(nom_service)


class AttributsCompte(TypedDict):
    """Dictionnaire d'attributs d'un compte Adonis"""
    # Les champs "groupe", "categorie", "fonction", et "organisme"
    # doivent correspondre aux libellés et sigles utilisés par Adonis.
    civilite:           Literal['M.', 'Mme']
    nom:                str
    prenom:             str
    date_naissance:     date
    groupe:             str
    categorie:          str
    fonction:           str
    organisme:          str
    date_expiration:    Optional[date]
    bureau:             Optional[str]
    telephone:          Optional[str]
    # NotRequired nécessite Python 3.11
    #date_expiration:   NotRequired[date]
    #bureau:            NotRequired[str]
    #telephone:         NotRequired[str]
    campus:             str
    batiment:           str


class Adonis():
    """Manipulations diverses de l'API REST Adonis"""
    def __init__(self, serveur: str=SERVEUR, cache_nomenclature: bool=True):
        self._session = SessionAdonis(serveur)
        self._groupes:       Dict[str, int]
        self._categories:    Dict[str, int]
        self._fonctions:     Dict[str, int]
        self._organismes:    Dict[str, int]
        self._campus:        Dict[str, int]
        self._batiments:     Dict[str, Dict[str, int]]
        self._services:      Dict[str, int]
        if cache_nomenclature:
            self._initialisation_nomenclature_depuis_cache()
        else:
            self._initialisation_nomenclature()

    def _nomenclature_adonis(self, requete: str, champ_nom: str,
                            champ_id: str, liste: List) -> Dict[str, int]:
        """Retourne un dictionnaire de nomenclatures Adonis intéressant le labo."""
        reponse = self._session.get(requete)
        if reponse.status_code == 200:
            items = reponse.json()
            items_labo = {}
            for item in items:
                if item[champ_nom] in liste:
                    items_labo[item[champ_nom]] = item[champ_id]
            return items_labo
        return {}

    def _campus_adonis(self) -> Dict[str, int]:
        """Retourne les numéros Adonis des campus."""
        campus = {}
        reponse = self._session.get('/campus')
        if reponse.status_code == 200:
            campus_adonis = {nom: Id for Id, nom in reponse.json().items()}
            for nom_campus in NOMENCLATURE['bâtiments']:
                if nom_campus in campus_adonis:
                    campus[nom_campus] = int(campus_adonis[nom_campus])
                else:
                    raise CampusInconnu(nom_campus)
        return campus

    def _batiments_adonis(self) -> Dict[str, Dict[str, int]]:
        """Retourne les numéros Adonis des bâtiments, par campus."""
        batiments = {}
        for nom_campus, campusId in self._campus.items():
            reponse = self._session.get(f'/buildings/campus/{campusId}')
            if reponse.status_code == 200:
                # La présence du mot "yoyo" est un bug d'Adonis.
                json_batiments = json.loads(reponse.text.replace('yoyo', ''))
                batiments_adonis = {b['libelle']: b['batimentId']
                                    for b in json_batiments}
                for nom_batiment in NOMENCLATURE['bâtiments'][nom_campus]:
                    if nom_batiment in batiments_adonis:
                        if nom_campus not in batiments:
                            batiments[nom_campus] = {}
                        batiments[nom_campus][nom_batiment] = int(batiments_adonis[nom_batiment])
                    else:
                        raise BatimentInconnu(nom_batiment)
        return batiments

    def _initialisation_nomenclature(self):
        """Affecte les identifiants de nomenclature avec l'API."""
        self._groupes = dict(
            self._nomenclature_adonis(
                requete=f'/groups/{ID_GROUPE_LABO}/children',
                champ_nom='sigle',
                champ_id='groupeId',
                liste=NOMENCLATURE['groupes']
            ),
            **{'FMJH': ID_GROUPE_FMJH}
        )
        self._categories = self._nomenclature_adonis(
            requete='/nomenclatures/categories',
            champ_nom='libelle',
            champ_id='categorieId',
            liste=NOMENCLATURE['categories']
        )
        self._fonctions = self._nomenclature_adonis(
            requete='/nomenclatures/functions',
            champ_nom='libelle',
            champ_id='fonctionLibelleId',
            liste=NOMENCLATURE['fonctions']
        )
        self._organismes = self._nomenclature_adonis(
            requete='/nomenclatures/organismes',
            champ_nom='sigle',
            champ_id='organismeId',
            liste=NOMENCLATURE['organismes']
        )
        self._campus = self._campus_adonis()
        self._batiments = self._batiments_adonis()
        self._services = {service: self._id_service(nom)
                          for service, nom in NOMENCLATURE['services'].items()}


    def _initialisation_nomenclature_depuis_cache(self):
        """Affecte les identifiants de nomenclature avec des valeurs en dur."""
        self._groupes = {
            'Bibliothèque':      94,
            'Pôle logistique':   9397,
            'Pôle informatique': 9396,
            'Pôle gestion':      9395,
            'Pôle enseignement': 9398,
            'TOPO':              945,
            'PS':                944,
            'AGA':               942,
            'ANEDP':             943,
            'ANH':               941,
            'FMJH':              ID_GROUPE_FMJH,
        }
        self._categories = {
            'Chercheur co-tutélaire':                               41,
            'Enseignant-Chercheur co-tutélaire':                    40,
            'Enseignant co-tutélaire':                              45,
            'Personnel co-tutélaire':                               39,
            'Personnel administratif ou technique co-tutélaire':    46,
        }
        self._fonctions = {
            'Enseignant-Chercheur':     134,
            'Chercheur':                80,
            'Enseignant':               132,
            'Ingénieur de Recherche':   146,
            'Gestionnaire':             138,
            'Informaticien':            145,
            'Agent Technique':          22,
            'Secrétaire pédagogique':   264,
            'Bibliothécaire':           47,
            'Doctorant':                122,
            'Etudiant':                 1,
            'Ingénieur':                146,
        }
        self._organismes = {
            'UPSa':     37,
            'CNRS':     6,
            'INRIA':    25,
            'IHES':     21,
            'AOPublic': 64,
            'AOPrive':  65,
        }
        self._campus = {
            'Bures-Orsay-Gif':    40,
        }
        self._batiments = {
            'Bures-Orsay-Gif': {
                '307':  291,
            },
        }
        self._services = {
            'labo':         61,
            'impression':   63,
        }

    def _recuperation_compte(self, compteId: int) -> JSONcompte:
        """Retourne le dictionnaire d'un compte."""
        reponse = self._session.get(f'/accounts/{compteId}/statuts,emails')
        if reponse.status_code == 200:
            return reponse.json()
        else:
            raise CompteInconnu(compteId)

    @staticmethod
    def _mot_de_passe_adonis() -> str:
        """Retourne un mot de passe généré selon les critères d'Adonis."""
        longueur = 20
        ponctuation = '!@#$%^&*?_<>~-=|/+,;.`[]{}'
        caracteres = ascii_letters + digits + ponctuation
        while True:
            mot_de_passe = random.choices(caracteres, k=longueur)
            ensemble = set(mot_de_passe)
            # On veut au moins une lettre, un chiffre, et un signe de ponctuation
            if (ensemble.intersection(ascii_letters)
                and ensemble.intersection(digits)
                and ensemble.intersection(ponctuation)):
                return ''.join(mot_de_passe)

    def _creation_compte(self, civilite: str, nom: str, prenom: str,
                        date_naissance: date, groupe: str, categorie: str,
                        fonction: str, organisme: str,
                        date_expiration: date=None, bureau: str=None,
                        telephone: str=None, batiment: str=BATIMENT_PAR_DEFAUT,
                        campus: str=CAMPUS_PAR_DEFAUT) -> JSONcompte:
        """Crée un compte Adonis."""
        creation_compte: Dict[str, Union[int, str]] = {
            'civilite':             civilite,
            'nom':                  nom,
            'prenom':               prenom,
            'date_naissance':       date_naissance.isoformat(),
            'groupeId':             self._groupes[groupe],
            'categorieId':          self._categories[categorie],
            'fonctionLibelleId':    self._fonctions[fonction],
            'password':             self._mot_de_passe_adonis(),
            'batimentId':           self._batiments[campus][batiment],
            'campusId':             self._campus[campus],
            'organismeId':          self._organismes[organisme],
        }
        if date_expiration:
            creation_compte['date_expiration'] = date_expiration.isoformat()
        if bureau:
            creation_compte['bureau'] = bureau
        if telephone:
            creation_compte['telephone'] = telephone

        reponse = self._session.post('/accounts', data=creation_compte)
        try:
            if reponse.status_code == 201:
                if 'error' not in reponse.json().keys():
                    compteId = reponse.json()['compteId']
                    compte = self._recuperation_compte(compteId)
                    logger.info('Création du compte ' + compte['identifiant'])
                    for serviceId in self._services.values():
                        self._session.post(f'/accounts/{compteId}/services/{serviceId}')
                    return compte
                else:
                    raise EchecCreationCompte(prenom, nom,
                                              reponse.json()['error']['message'])
            elif reponse.status_code == 400:
                raise EchecCreationCompte(prenom, nom,
                                          reponse.json()['error']['message'])
            else:
                raise EchecCreationCompte(prenom, nom)
        except EchecCreationCompte as erreur:
            logger.warning(erreur)
            raise

    @staticmethod
    def _comparaison_noms_adonis(nom1: str, nom2: str) -> bool:
        """Retourne True si deux noms sont identiques une fois normalisés."""
        return (unidecode(nom1).lower().replace(' ', '-')
                == unidecode(nom2).lower().replace(' ', '-'))

    def _recherche_compte(self, nom: str, prenom: str, date_naissance: date,
                         **_) -> Optional[JSONcompte]:
        """Retourne un JSON de compte Adonis si il existe."""
        if "'" in nom:
            recherche = nom.replace("'", '')
        elif ' ' in nom or '-' in nom:
            recherche = nom
        else:
            recherche = nom + ' ' + prenom
        reponse = self._session.get('/accounts', params={'search': recherche})
        if reponse.status_code == 200:
            if type(reponse.json()[0]) == list:
                comptes = reponse.json()[0]
            else:
                comptes = reponse.json()
            for compte in comptes:
                if (self._comparaison_noms_adonis(compte['nom'], nom)
                    and self._comparaison_noms_adonis(compte['prenom'], prenom)
                    and date.fromisoformat(compte['date_naissance']) ==  date_naissance):
                    return self._recuperation_compte(compte['compteId'])

    @staticmethod
    def email_compte(compte: JSONcompte) -> Optional[str]:
        """Retourne l'email d'un compte."""
        for email in compte['emails']:
            if email['type'] == TYPE_EMAIL_PRINCIPAL:
                return email['email']

    def _groupe_est_dans_statut(self, groupe: str, statut: JSONstatut) -> bool:
        """Retourne True si le statut est affecté au groupe donné."""
        groupes = [g['groupeId'] for g in statut['groupes']]
        return self._groupes[groupe] in groupes

    @staticmethod
    def _statut_pertinent(compte: JSONcompte, categorie: str) -> Optional[JSONstatut]:
        """Retourne le statut d'un compte correspondant à sa fonction."""
        for statut in compte['statuts']:
            if statut['categorie']['libelle'] == categorie:
                return statut

    def _ajout_statut(self, compte: JSONcompte, categorie: str, groupe: str,
                     organisme: str, date_expiration: date=None,
                     **_) -> Optional[int]:
        """Ajoute un statut à un compte."""
        statut: Dict[str, Union[int, str]] = {
            'categorieId': self._categories[categorie],
            'organismeId': self._organismes[organisme],
            'groupeId': self._groupes[groupe],
        }
        if date_expiration:
            statut['date_fin'] = date_expiration.isoformat()
        compteId = compte['compteId']
        reponse = self._session.post(f'/accounts/{compteId}/statut', data=statut)
        login = compte['identifiant']
        if reponse.status_code == 201:
            logger.info(f'Ajout du statut "{categorie}" au compte {login}')
            return reponse.json()['StatutId']
        else:
            logger.warning(f'Échec d\'ajout du statut "{categorie}" '
                           f"au compte {login}")

    def _affectation_statut(self, compte: JSONcompte, statut: JSONstatut, groupe: str,
                            categorie: str, **_):
        """Affecte un groupe à un statut."""
        compteId = compte['compteId']
        statutId = statut['statutId']
        groupeId = self._groupes[groupe]
        reponse = self._session.post(f'/accounts/{compteId}/statut/{statutId}'
                                     f'/group/{groupeId}')
        if reponse.status_code != 201:
            login = compte['identifiant']
            logger.warning(f"Échec d'ajout du groupe {groupe} au statut "
                           f'"{categorie}" du compte {login}')


    def _fonction_est_dans_statut(self, statut: JSONstatut, fonction: str, groupe: str,
                                  **_) -> bool:
        """Retourne True si la fonction est affectée au statut, dans le bon groupe."""
        for groupe_statut in statut['groupes']:
            if groupe_statut['groupeId'] == self._groupes[groupe]:
                if fonction in [f['libelle'] for f in groupe_statut['fonctions']]:
                    return True
        return False

    def _ajout_fonction(self, compte: JSONcompte, statutId: int, groupe: str,
                        fonction: str, categorie: str, bureau: str=None,
                        telephone: str=None, batiment: str=BATIMENT_PAR_DEFAUT,
                        campus: str=CAMPUS_PAR_DEFAUT, **_):
        """Ajoute une fonction à un compte."""
        # La fonction attendue est une fonction existant dans Adonis.
        function: Dict[str, Union[int, str]] = {
            'fonctionLibelleId': self._fonctions[fonction],
            'batimentId': self._batiments[campus][batiment],
            'campusId': self._campus[campus],
        }
        if bureau:
            function['bureau'] = bureau
        if telephone:
            function['telephone'] = telephone
        compteId = compte['compteId']
        groupeId = self._groupes[groupe]
        reponse = self._session.post(f'/accounts/{compteId}/statut/{statutId}'
                                     f'/group/{groupeId}/function', data=function)
        login = compte['identifiant']
        if reponse.status_code == 201:
            logger.debug(f'Ajout de la fonction "{fonction}" au compte {login}')
        else:
            logger.warning(f'Échec d\'ajout de la fonction "{fonction}" au groupe '
                           f'{groupe} du statut "{categorie}" du compte {login}')

    def creation_ou_affectation_compte(self, attributs: AttributsCompte) -> JSONcompte:
        """Crée ou affecte correctement un compte depuis un dictionnaire."""
        if compte := self._recherche_compte(**attributs):
            if statut := self._statut_pertinent(compte, attributs['categorie']):
                if self._groupe_est_dans_statut(attributs['groupe'], statut):
                    if not self._fonction_est_dans_statut(statut, **attributs):
                        self._ajout_fonction(compte, statut['statutId'], **attributs)
                else:
                    self._affectation_statut(compte, statut, **attributs)
                    self._ajout_fonction(compte, statut['statutId'], **attributs)
            else:
                if statutId := self._ajout_statut(compte, **attributs):
                    self._ajout_fonction(compte, statutId, **attributs)
        else:
            compte = self._creation_compte(**attributs)
        return compte

    def _id_compte(self, login: str) -> int:
        """Retourne le compteId pour un login donné."""
        reponse = self._session.get('/accounts', params={'identifiant': login})
        if reponse.ok:
            return reponse.json()['compteId']
        else:
            raise LoginInconnu(login)

    def sigle_groupe_compte(self, login: str) -> str:
        """Retourne le sigle de groupe d'un compte."""
        compte = self._recuperation_compte(self._id_compte(login))
        statuts = compte['statuts']
        for statut in statuts:
            for groupe in statut['groupes']:
                if groupe['sigle'] in self._groupes:
                    return groupe['sigle']
        raise GroupeInconnu(login)

    def mise_a_jour_compte(self, login: str, categorie: str,
                           date_expiration: Optional[date], fonction: str,
                           bureau: Optional[str], telephone: Optional[str],
                           **_):
        """Mets à jour un compte depuis des attributs."""
        # Attributs mis à jour : date d'expiration uniquement pour le moment
        try:
            compteId = self._id_compte(login)
        except LoginInconnu:
            return
        compte = self._recuperation_compte(compteId)
        statut = self._statut_pertinent(compte, categorie)
        if not statut:
            return
        statutId = statut['statutId']
        if date_fin := statut['date_fin']:
            date_fin = date.fromisoformat(date_fin.split()[0])
        if date_fin != date_expiration:
            reponse = self._session.put(
                                f'/accounts/{compteId}/statut/{statutId}',
                                data={'date_fin': date_expiration})
            login = compte['identifiant']
            if reponse.status_code == 200:
                logger.info("Mise à jour de la date d'expiration du "
                            f'compte {login} : {date_fin} -> {date_expiration}')
            else:
                logger.warning('Échec de la mise à jour de date '
                               f"d'expiration du compte {login}")

    def modification_mot_de_passe(self, login: str, mot_de_passe: str):
        """Modifie un mot de passe."""
        compteId = self._id_compte(login)
        reponse = self._session.post(f'/accounts/{compteId}/password',
                                     data={'password': mot_de_passe})
        try:
            if reponse.status_code == 201:
                logger.info(f'Modification du mot de passe du compte {login}')
            else:
                raise EchecModificationMotDePasse(login)
        except EchecModificationMotDePasse as erreur:
            logger.warning(erreur)
            raise

    def _id_service(self, nom_service: str) -> int:
        """Retourne le serviceId pour un nom de service donné."""
        for service in self._session.get('/services').json():
            if service['service'] == nom_service:
                return service['serviceId']
        else:
            raise ServiceInconnu(nom_service)

    def suppression_service(self, nom_service: str, login: str):
        """Supprime un service d'un compte."""
        serviceId = self._id_service(nom_service)
        compteId = self._id_compte(login)
        reponse = self._session.delete(f'/accounts/{compteId}/services/{serviceId}')
        if reponse.status_code == 200:
            logger.info(f'Service {nom_service} supprimé du compte {login}')
        else:
            logger.warning('Échec de la suppression du service '
                           f'{nom_service} du compte {login}')
