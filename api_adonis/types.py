"""Types des données JSON retournée par l'API Adonis"""
from typing import List, Literal, TypedDict


class JSONemail(TypedDict):
    """JSON d'email de compte retourné par Adonis"""
    email:  str
    type:   int


class JSONcategorie(TypedDict):
    """JSON de catégorie de statut de compte retourné par Adonis"""
    categorieId:    int
    libelle:        str


class JSONbatiment(TypedDict):
    """JSON de bâtiment retourné par Adonis"""
    batimentId: int
    batiment:   str
    campus:     str


class JSONfonction(TypedDict):
    """JSON de fonction de compte retourné par Adonis"""
    fonctionId: int
    libelle:    str
    bureau:     str
    batiment:   JSONbatiment


class JSONgroupe(TypedDict):
    """JSON de groupe de compte retournée par Adonis"""
    groupeId:   int
    sigle:      str
    libelle:    str
    fonctions:  List[JSONfonction]


class JSONstatut(TypedDict):
    """JSON de statut de compte retourné par Adonis"""
    statutId:   int
    date_fin:   str
    categorie:  JSONcategorie
    groupes:    List[JSONgroupe]


class JSONcompte_simplifie(TypedDict):
    """JSON de compte retourné par la recherche d'Adonis"""
    # La requête "GET /accounts" renvoie une liste de JSON de ce type.
    compteId:       int
    identifiant:    str
    civilite:       Literal['M.', 'Mme']
    nom:            str
    prenom:         str
    date_naissance: str


class JSONcompte(JSONcompte_simplifie):
    """JSON de compte retourné par Adonis"""
    emails:         List[JSONemail]
    statuts:        List[JSONstatut]


class JSONservice(TypedDict):
    """JSON de service retourné par Adonis"""
    service:    str
    serviceId:   int
