from api_adonis.api_adonis import NOMENCLATURE, Adonis, ID_GROUPE_FMJH


def test_nomenclature_groupes(adonis: Adonis, groupes_adonis):
    groupes = dict(
        {g['sigle']: g['groupeId'] for g in groupes_adonis},
        **{'FMJH': ID_GROUPE_FMJH},
    )
    assert adonis._groupes == groupes


def test_nomenclature_categories(adonis: Adonis, categories_adonis):
    assert adonis._categories == {c['libelle']: c['categorieId']
                                  for c in categories_adonis}


def test_nomenclature_fonctions(adonis: Adonis, fonctions_adonis):
    assert adonis._fonctions == {f['libelle']: f['fonctionLibelleId']
                                 for f in fonctions_adonis}


def test_nomenclature_organimes(adonis: Adonis, organismes_adonis):
    assert adonis._organismes == {o['sigle']: o['organismeId']
                                  for o in organismes_adonis}


def test_nomenclature_campus(adonis: Adonis, campus_adonis):
    assert adonis._campus == {nom: int(Id)
                              for Id, nom in campus_adonis.items()}


def test_nomenclature_batiments(adonis: Adonis, batiments_adonis):
    assert adonis._batiments == batiments_adonis


def test_nomenclature_services(adonis: Adonis, services_adonis):
    services = {nom: libelle
                for libelle, nom in NOMENCLATURE['services'].items()}
    assert adonis._services == {services[s['service']]: s['serviceId']
                                for s in services_adonis}
