from datetime import date
from typing import List
from urllib.parse import urlencode

import pytest
from requests.models import PreparedRequest

from api_adonis.api_adonis import (SERVEUR, Adonis, EchecModificationMotDePasse,
                                   GroupeInconnu, LoginInconnu, ServiceInconnu)
from api_adonis.types import JSONcompte, JSONservice, JSONstatut


def test_id_compte(requests_mock, adonis: Adonis, compte_adonis: JSONcompte,
                   recherche_compte_par_identifiant):
    """Teste Adonis._id_compte()."""
    login = compte_adonis['identifiant']
    
    # Compte trouvé
    assert adonis._id_compte(login)== compte_adonis['compteId']

    # Compte non trouvé
    requests_mock.get(
        SERVEUR + f'/accounts?identifiant={login}',
        status_code=404,
    )
    with pytest.raises(LoginInconnu, match=fr'{login}'):
        adonis._id_compte(login)


def test_id_service(requests_mock, adonis: Adonis, services_adonis):
    """Teste la réussite de Adonis._id_service()."""
    service = services_adonis[0]
    serviceId = adonis._id_service(service['service'])
    assert serviceId == service['serviceId']


def test_id_service_inconnu(requests_mock, adonis: Adonis, services_adonis):
    """Teste l'échec de Adonis._id_service()."""
    service = 'service inexistant'
    with pytest.raises(ServiceInconnu, match=fr'{service}'):
        adonis._id_service(service)


def test_sigle_groupe_compte(requests_mock, adonis: Adonis,
                             compte_adonis: JSONcompte, recuperation_compte,
                             recherche_compte_par_identifiant):
    """Teste Adonis.sigle_groupe_compte()."""
    login = compte_adonis['identifiant']

    # Groupe trouvé
    groupe = adonis.sigle_groupe_compte(login)
    assert groupe == compte_adonis['statuts'][0]['groupes'][0]['sigle']

    # Groupe non trouvé
    compte_adonis['statuts'][0]['groupes'][0]['sigle'] = 'groupe inexistant'
    with pytest.raises(GroupeInconnu, match=fr'{login}'):
        groupe = adonis.sigle_groupe_compte(login)


def test_mise_a_jour_compte(requests_mock, adonis: Adonis, caplog,
                            recherche_compte_par_identifiant,
                            recuperation_compte,
                            compte_adonis: JSONcompte,
                            statut_adonis: JSONstatut):
    compteId = compte_adonis['compteId']
    statutId = statut_adonis['statutId']
    login = compte_adonis['identifiant']
    categorie = statut_adonis['categorie']['libelle']
    date_expiration = date(day=1, month=1, year=2025)
    nouveau_statut = {'date_fin': date_expiration}

    def match_body_put_account(request: PreparedRequest) -> bool:
        return request.body == urlencode(nouveau_statut)
    requests_mock.put(
        SERVEUR + f'/accounts/{compteId}/statut/{statutId}',
        additional_matcher=match_body_put_account,
        status_code=200,
    )

    # Changement de date de fin
    adonis.mise_a_jour_compte(login, categorie, date_expiration, '', '', '')
    for record in caplog.records:
        assert record.levelname != 'WARNING'

    # Nouvelle date de fin identique à l'actuelle
    adonis.mise_a_jour_compte(login, categorie,
                              date.fromisoformat(statut_adonis['date_fin']),
                              '', '', '')
    for record in caplog.records:
        assert record.levelname != 'WARNING'
    # Si la date ne change pas, aucune requête ne doit partir.
    assert requests_mock.last_request.method != 'PUT'

    # Échec de mise à jour
    requests_mock.put(
        SERVEUR + f'/accounts/{compteId}/statut/{statutId}',
        status_code=400,
    )
    adonis.mise_a_jour_compte(login, categorie, date_expiration, '', '', '')
    assert caplog.records[-1].levelname == 'WARNING'


def test_modification_mot_de_passe(requests_mock, adonis: Adonis, caplog,
                                   recherche_compte_par_identifiant,
                                   compte_adonis: JSONcompte):
    """Teste Adonis.modification_mot_de_passe()."""
    login = compte_adonis['identifiant']
    mot_de_passe = 'un mot de passe'

    # Changement de mot de passe réussi
    compteId = compte_adonis['compteId']
    data_mot_de_passe = {'password': mot_de_passe}
    def match_body_post_account(request: PreparedRequest) -> bool:
        return request.body == urlencode(data_mot_de_passe)
    requests_mock.post(
        SERVEUR + f'/accounts/{compteId}/password',
        additional_matcher=match_body_post_account,
        status_code=201,
    )
    try:
        adonis.modification_mot_de_passe(login, mot_de_passe)
    except EchecModificationMotDePasse:
        assert False

    # Échec du changement de mot de passe
    requests_mock.post(
        SERVEUR + f'/accounts/{compteId}/password',
        status_code=401,
    )
    with pytest.raises(EchecModificationMotDePasse, match=fr'{login}'):
        adonis.modification_mot_de_passe(login, mot_de_passe)


def test_suppression_service(requests_mock, adonis: Adonis, caplog,
                             recherche_compte_par_identifiant,
                             services_adonis: List[JSONservice],
                             compte_adonis: JSONcompte):
    nom_service = services_adonis[0]['service']
    serviceId = services_adonis[0]['serviceId']
    login = compte_adonis['identifiant']
    compteId = compte_adonis['compteId']


    # Suppression de service réussie
    requests_mock.delete(
        SERVEUR + f'/accounts/{compteId}/services/{serviceId}',
        status_code=200,
    )
    adonis.suppression_service(nom_service, login)
    for record in caplog.records:
        assert record.levelname != 'WARNING'

    # Échec de suppression de service
    requests_mock.delete(
        SERVEUR + f'/accounts/{compteId}/services/{serviceId}',
        status_code=401,
    )
    adonis.suppression_service(nom_service, login)
    assert caplog.records[-1].levelname == 'WARNING'
