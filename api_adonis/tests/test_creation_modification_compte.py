from datetime import date
import random
from urllib.parse import urlencode

import pytest
from requests.models import PreparedRequest

from api_adonis.api_adonis import SERVEUR, Adonis, CompteInconnu, EchecCreationCompte
from api_adonis.types import JSONcompte, JSONcompte_simplifie, JSONfonction, JSONstatut


def test_recuperation_compte(requests_mock, adonis: Adonis, recuperation_compte,
                             compte_adonis: JSONcompte):
    """Teste la réussite de Adonis._recuperation_compte()."""
    compte = adonis._recuperation_compte(compte_adonis['compteId'])
    assert compte == compte_adonis


def test_recuperation_compte_inexistant(requests_mock, adonis: Adonis):
    """Teste l'échec de Adonis._recuperation_compte()."""
    # Id de comtpe non déclaré dans les tests
    compteId = 123

    requests_mock.get(
        SERVEUR + f'/accounts/{compteId}/statuts,emails',
        status_code=404,
    )

    with pytest.raises(CompteInconnu, match=fr'{compteId}'):
        adonis._recuperation_compte(compteId)


def test_recherche_compte(requests_mock, adonis: Adonis, recuperation_compte,
                          compte_adonis_simplifie: JSONcompte_simplifie,
                          compte_adonis: JSONcompte):
    """Teste Adonis._recherche_compte()."""
    NOM = 'Massot'
    PRENOM = 'Bernard'

    requests_mock.get(
        SERVEUR + f'/accounts?search={NOM}+{PRENOM}',
        status_code=200,
        json=[compte_adonis_simplifie],
    )

    # Avec la bonne date de naissance
    compte = adonis._recherche_compte(NOM, PRENOM,
                                      date(day=1, month=1, year=1970))
    assert compte == compte_adonis
    # Avec une mauvaise date de naissance
    compte = adonis._recherche_compte(NOM, PRENOM,
                                      date(day=31, month=12, year=1970))
    assert compte is None


def test_recherche_compte_inexistant(requests_mock, adonis: Adonis):
    """Teste Adonis._recherche_compte() avec un compte inexistant."""
    NOM = 'Dupont'
    PRENOM = 'Bernard'

    requests_mock.get(
        SERVEUR + f'/accounts?search={NOM}+{PRENOM}',
        status_code=404,
    )

    compte = adonis._recherche_compte(NOM, PRENOM,
                                      date(day=1, month=1, year=1970))
    assert compte is None


def test_groupe_est_dans_statut(requests_mock, adonis: Adonis, statut_adonis: JSONstatut):
    """Teste Adonis._groupe_est_dans_statut()."""
    assert adonis._groupe_est_dans_statut('Pôle informatique', statut_adonis)


def test_ajout_statut(requests_mock, adonis: Adonis, compte_adonis: JSONcompte):
    """Teste Adonis._ajout_statut()."""
    compteId = 1234
    categorie = 'Enseignant-Chercheur co-tutélaire'
    organisme = 'CNRS'
    groupe = 'TOPO'

    # Ajout de statut valide.
    statutId = 1
    statut = {
        'categorieId':  1,
        'organismeId':  1,
        'groupeId':     5,
    }
    def match_body_post_accounts(request: PreparedRequest) -> bool:
        return request.body == urlencode(statut)
    requests_mock.post(
        SERVEUR + f'/accounts/{compteId}/statut',
        additional_matcher=match_body_post_accounts,
        status_code=201,
        json={'StatutId': statutId}
    )
    nouveau_statut_id = adonis._ajout_statut(compte_adonis, categorie, groupe,
                                             organisme)
    assert nouveau_statut_id == statutId
    
    # Ajout de statut interdit.
    requests_mock.post(
        SERVEUR + f'/accounts/{compteId}/statut',
        status_code=401,
    )
    nouveau_statut_id = adonis._ajout_statut(compte_adonis, categorie, groupe,
                                             organisme)
    assert nouveau_statut_id == None


def test_affectation_statut(requests_mock, adonis: Adonis, caplog,
                            compte_adonis: JSONcompte,
                            statut_adonis: JSONstatut):
    """Teste Adonis._affectation_statut()."""
    compteId = compte_adonis['compteId']
    statutId = statut_adonis['statutId']
    groupeId = 5

    # Affectation valide.
    requests_mock.post(
        SERVEUR + f'/accounts/{compteId}/statut/{statutId}/group/{groupeId}',
        status_code=201,
    )
    adonis._affectation_statut(compte_adonis, statut_adonis, 'TOPO',
                               statut_adonis['categorie']['libelle'])
    for record in caplog.records:
        assert record.levelname != 'WARNING'

    # Affectation interdite.
    requests_mock.post(
        SERVEUR + f'/accounts/{compteId}/statut/{statutId}/group/{groupeId}',
        status_code=401,
    )
    adonis._affectation_statut(compte_adonis, statut_adonis, 'TOPO',
                               statut_adonis['categorie']['libelle'])
    assert caplog.records[-1].levelname == 'WARNING'


def test_fonction_est_dans_statut(requests_mock, adonis: Adonis,
                                  statut_adonis: JSONstatut):
    """Teste Adonis._fonction_est_dans_statut()."""
    # Bonne fonction, bon groupe
    assert adonis._fonction_est_dans_statut(statut_adonis, 'Informaticien', 
                                            'Pôle informatique')
    # Bonne fonction, mauvais groupe
    assert not adonis._fonction_est_dans_statut(statut_adonis, 'Informaticien',
                                                'TOPO')
    # Mauvaise fonction, bon groupe
    assert not adonis._fonction_est_dans_statut(statut_adonis, 'Enseignant',
                                                'Pôle informatique')


def test_ajout_fonction(requests_mock, adonis: Adonis, caplog,
                        compte_adonis: JSONcompte,
                        statut_adonis: JSONstatut,
                        fonction_adonis: JSONfonction,
                        campus_adonis):
    """Teste Adonis._ajout_fonction()."""
    compteId = 1234
    statutId = 1234
    groupe = statut_adonis['groupes'][0]['sigle']
    groupeId = 2
    fonction = fonction_adonis['libelle']
    categorie = statut_adonis['categorie']['libelle']
   
    # Ajout de fonction valide
    # Pour pyright :
    campusId = '0'
    for campusId, campus in campus_adonis.items():
        if campus == fonction_adonis['batiment']['campus']:
            break
    function = {
        'fonctionLibelleId':    fonction_adonis['fonctionId'],
        'batimentId':           fonction_adonis['batiment']['batimentId'],
        'campusId':             int(campusId),
    }
    def match_body_post_function(request: PreparedRequest) -> bool:
        return request.body == urlencode(function)
    requests_mock.post(
        SERVEUR + f'/accounts/{compteId}/statut/{statutId}'
                  f'/group/{groupeId}/function',
        additional_matcher=match_body_post_function,
        status_code=201,
    )
    adonis._ajout_fonction(compte_adonis, statutId, groupe, fonction, categorie)
    for record in caplog.records:
        assert record.levelname != 'WARNING'
    
    # Ajout de fonction invalide
    requests_mock.post(
        SERVEUR + f'/accounts/{compteId}/statut/{statutId}'
                  f'/group/{groupeId}/function',
        status_code=401,
    )
    adonis._ajout_fonction(compte_adonis, statutId, groupe, fonction, categorie)
    assert caplog.records[-1].levelname == 'WARNING'


def test_creation_compte(requests_mock, adonis: Adonis, recuperation_compte,
                         compte_adonis: JSONcompte,
                         fonction_adonis: JSONfonction,
                         statut_adonis: JSONstatut, campus_adonis,
                         services_adonis, mot_de_passe_adonis_seed_0):
    """Teste Adonis._creation_compte()."""
    compteId = compte_adonis['compteId']
    civilite = compte_adonis['civilite']
    nom = compte_adonis['nom']
    prenom = compte_adonis['prenom']
    date_naissance = date.fromisoformat(compte_adonis['date_naissance'])
    groupe = statut_adonis['groupes'][0]['sigle']
    groupeId = statut_adonis['groupes'][0]['groupeId']
    categorie = statut_adonis['categorie']['libelle']
    categorieId = statut_adonis['categorie']['categorieId']
    fonction = fonction_adonis['libelle']
    fonctionId = fonction_adonis['fonctionId']
    batimentId = fonction_adonis['batiment']['batimentId']
    # Pour pyright :
    campusId = '0'
    for campusId, campus in campus_adonis.items():
        if campus == fonction_adonis['batiment']['campus']:
            break
    organisme = 'CNRS'
    organismeId = 1
    creation_compte = {
        'civilite':             civilite,
        'nom':                  nom,
        'prenom':               prenom,
        'date_naissance':       date_naissance,
        'groupeId':             groupeId,
        'categorieId':          categorieId,
        'fonctionLibelleId':    fonctionId,
        'password':             mot_de_passe_adonis_seed_0,
        'batimentId':           batimentId,
        'campusId':             campusId,
        'organismeId':          organismeId,
    }
    def match_body_post_account(request: PreparedRequest):
        return request.body == urlencode(creation_compte)

    # Réussite de création de compte
    requests_mock.post(
        SERVEUR + '/accounts',
        additional_matcher=match_body_post_account,
        status_code=201,
        json=compte_adonis,
    )
    for service in services_adonis:
        serviceId = service['serviceId']
        requests_mock.post(
            SERVEUR + f'/accounts/{compteId}/services/{serviceId}',
            status_code=201,
        )
    random.seed(0)
    compte = adonis._creation_compte(civilite, nom, prenom, date_naissance,
                                     groupe, categorie, fonction, organisme)
    assert compte == compte_adonis

    # Échec de création de compte (bad request)
    erreur_adonis = 'demande invalide'
    requests_mock.post(
        SERVEUR + '/accounts',
        additional_matcher=match_body_post_account,
        status_code=400,
        json={'error': {'message': erreur_adonis}}
    )
    random.seed(0)
    with pytest.raises(EchecCreationCompte, match=fr'{nom}.*{erreur_adonis}'):
        adonis._creation_compte(civilite, nom, prenom, date_naissance,
                                groupe, categorie, fonction, organisme)
    
    # Échec de création de compte (unauthorized)
    requests_mock.post(
        SERVEUR + '/accounts',
        status_code=401,
    )
    random.seed(0)
    with pytest.raises(EchecCreationCompte, match=fr'{nom}'):
        adonis._creation_compte(civilite, nom, prenom, date_naissance,
                                groupe, categorie, fonction, organisme)
