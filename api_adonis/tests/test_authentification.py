from urllib.parse import urlencode
import random

from requests import PreparedRequest

from api_adonis.api_adonis import SessionAdonis, SERVEUR
from api_adonis.identifiants_api_adonis import USERNAME, PASSWORD


CAMPUS = {
	'40': 'Orsay',
	'4':  'BPC Metro',
	'44': 'Kremlin-Bicêtre',
	'41': 'Sceaux',
	'42': 'Cachan',
	'45': 'Autres',
}
# Valeur aléatoire arbitraire
ACCESS_TOKEN = 'AbCd'
# Valeur calculée par AdonisRestAuth, uniquement après random.seed(0),
# obtenue empiriquement.
STATE = 'vtkgnkuhmpxnhtqgxzvxisxrmclpxzmw'


def test_authentification(requests_mock):
    """Teste l'authentification via une requête simple et anodine."""
    data = {'username': USERNAME, 'password': PASSWORD, 'state': STATE}

    def match_body_signin(request: PreparedRequest) -> bool:
        return request.body == urlencode(data)

    requests_mock.post(
        SERVEUR + '/auth/signin',
        additional_matcher=match_body_signin,
        status_code=201,
        json={'access_token': ACCESS_TOKEN, 'state': STATE},
    )

    requests_mock.get(
        SERVEUR + '/campus',
        request_headers={'Authorization': f'Bearer {ACCESS_TOKEN}',
                         'Content-Type':   'application/x-www-form-urlencoded'},
        status_code=200,
        json=CAMPUS,
    )

    # Pour avoir la bonne valeur de "state"
    random.seed(0)
    session = SessionAdonis()
    reponse = session.get('/campus')

    assert reponse.status_code == 200
    assert reponse.json() == CAMPUS
