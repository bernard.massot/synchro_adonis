from typing import List

from pytest import fixture

from api_adonis.api_adonis import ID_GROUPE_LABO, SERVEUR, Adonis
from api_adonis.types import (JSONcompte, JSONcompte_simplifie, JSONfonction,
                              JSONservice, JSONstatut)


@fixture
def authentification(requests_mock):
    """Authentification API REST réussie"""
    # Valeurs aléatoires arbitraires pour le token et state
    ACCESS_TOKEN = 'AbCd'
    STATE = 'abcd'

    requests_mock.post(
        SERVEUR + '/auth/signin',
        status_code=201,
        json={'access_token': ACCESS_TOKEN, 'state': STATE},
    )


# Les fixtures de nomenclature servent à la fois à préparer les requêtes
# requests_mock, utiles à l'ensemble des tests, et à renvoyer des
# valeurs utilisables par les tests spécifiques de nomenclature.
#
# On utilise enumerate() pour générer les identifiants Adonis des différentes
# entités, pour garantir leur unicité.

@fixture
def groupes_adonis(requests_mock, authentification):
    groupes_adonis = [
        {'sigle': 'Bibliothèque',       'groupeId': 0},
        {'sigle': 'Pôle logistique',    'groupeId': 1},
        {'sigle': 'Pôle informatique',  'groupeId': 2},
        {'sigle': 'Pôle gestion',       'groupeId': 3},
        {'sigle': 'Pôle enseignement',  'groupeId': 4},
        {'sigle': 'TOPO',               'groupeId': 5},
        {'sigle': 'PS',                 'groupeId': 6},
        {'sigle': 'AGA',                'groupeId': 7},
        {'sigle': 'ANEDP',              'groupeId': 8},
        {'sigle': 'ANH',                'groupeId': 9},
    ]

    requests_mock.get(
        SERVEUR + f'/groups/{ID_GROUPE_LABO}/children',
        status_code=200,
        json=groupes_adonis,
    )

    return groupes_adonis


@fixture
def categories_adonis(requests_mock, authentification):
    categories_adonis = [
        {'libelle':     'Chercheur co-tutélaire',                               'categorieId': 0},
        {'libelle':     'Enseignant-Chercheur co-tutélaire',                    'categorieId': 1},
        {'libelle':     'Enseignant co-tutélaire',                              'categorieId': 2},
        {'libelle':     'Personnel co-tutélaire',                               'categorieId': 3},
        {'libelle':     'Personnel administratif ou technique co-tutélaire',    'categorieId': 4},
    ]

    requests_mock.get(
        SERVEUR + '/nomenclatures/categories',
        status_code=200,
        json=categories_adonis,
    )

    return categories_adonis


@fixture
def fonctions_adonis(requests_mock, authentification):
    fonctions_adonis = [
        {'libelle': 'Enseignant-Chercheur',     'fonctionLibelleId': 0},
        {'libelle': 'Chercheur',                'fonctionLibelleId': 1},
        {'libelle': 'Enseignant',               'fonctionLibelleId': 2},
        {'libelle': 'Ingénieur de Recherche',   'fonctionLibelleId': 3},
        {'libelle': 'Gestionnaire',             'fonctionLibelleId': 4},
        {'libelle': 'Informaticien',            'fonctionLibelleId': 5},
        {'libelle': 'Agent Technique',          'fonctionLibelleId': 6},
        {'libelle': 'Secrétaire pédagogique',   'fonctionLibelleId': 7},
        {'libelle': 'Bibliothécaire',           'fonctionLibelleId': 8},
        {'libelle': 'Doctorant',                'fonctionLibelleId': 9},
        {'libelle': 'Etudiant',                 'fonctionLibelleId': 10},
        {'libelle': 'Ingénieur',                'fonctionLibelleId': 11},
    ]

    requests_mock.get(
        SERVEUR + '/nomenclatures/functions',
        status_code=200,
        json=fonctions_adonis,
    )

    return fonctions_adonis


@fixture
def organismes_adonis(requests_mock, authentification):
    organismes_adonis = [
        {'sigle': 'UPSa',       'organismeId': 0},
        {'sigle': 'CNRS',       'organismeId': 1},
        {'sigle': 'INRIA',      'organismeId': 2},
        {'sigle': 'IHES',       'organismeId': 3},
        {'sigle': 'AOPublic',   'organismeId': 4},
        {'sigle': 'AOPrive',    'organismeId': 5},
    ]

    requests_mock.get(
        SERVEUR + '/nomenclatures/organismes',
        status_code=200,
        json=organismes_adonis,
    )

    return organismes_adonis


@fixture
def campus_adonis(requests_mock, authentification):
    campus_adonis = {'0': 'Orsay'}

    requests_mock.get(
        SERVEUR + '/campus',
        status_code=200,
        json=campus_adonis,
    )

    return campus_adonis


@fixture
def batiments_adonis(requests_mock, authentification, campus_adonis):
    batiments_adonis = {'Orsay': [
                            {'libelle': '307', 'batimentId': 0},
                       ]}

    batiments = {}
    for campusId, campus in campus_adonis.items():
        batiments_campus = batiments_adonis[campus]

        requests_mock.get(
            SERVEUR + f'/buildings/campus/{campusId}',
            status_code=200,
            json=batiments_campus,
        )
        batiments[campus] = {b['libelle']: b['batimentId']
                             for b in batiments_campus}

    return batiments


@fixture
def services_adonis(requests_mock, authentification) -> List[JSONservice]:
    services_adonis: List[JSONservice] = [
        {'service': 'lmo',          'serviceId': 0},
        {'service': 'impression',   'serviceId': 1},
    ]

    requests_mock.get(
        SERVEUR + '/services',
        status_code=200,
        json=services_adonis,
    )

    return services_adonis


@fixture
def adonis(requests_mock, groupes_adonis, categories_adonis, fonctions_adonis,
           organismes_adonis, campus_adonis, batiments_adonis,
           services_adonis) -> Adonis:
    """Instance d'API Adonis"""
    return Adonis(cache_nomenclature=False)


@fixture
def fonction_adonis() -> JSONfonction:
    fonction: JSONfonction = {
        'libelle':    'Informaticien',
        'fonctionId': 5,
        'bureau':     '2S1',
        'batiment':   {'batimentId':  0,
                       'batiment':    '307',
                       'campus':      'Orsay'},
    }
    return fonction


@fixture
def statut_adonis(fonction_adonis: JSONfonction) -> JSONstatut:
    statut: JSONstatut = {
        'categorie': {'libelle':      'Personnel co-tutélaire',
                      'categorieId':  3},
        'groupes': [{'fonctions': [fonction_adonis],
                     'groupeId':  2,
                     'libelle':   'Pôle informatique',
                     'sigle':     'Pôle informatique'}],
        'statutId': 1234,
        'date_fin': '2030-01-01',
    }
    return statut


@fixture
def compte_adonis_simplifie() -> JSONcompte_simplifie:
    """Dict de compte, tel que renvoyé par la recherche d'Adonis"""
    compte: JSONcompte_simplifie = {
        'compteId':         1234,
        'identifiant':      'bernard.massot',
        'civilite':         'M.',
        'nom':              'Massot',
        'prenom':           'Bernard',
        'date_naissance':   '1970-01-01',
    }
    return compte


@fixture
def compte_adonis(compte_adonis_simplifie, statut_adonis) -> JSONcompte:
    """Dict de compte, tel que renvoyé par Adonis"""
    compte = compte_adonis_simplifie
    compte['emails'] = [{'email':  'bernard.massot@universite-paris-saclay.fr',
                         'type':    0},
                        {'email':   'bernard.massot@u-psud.fr',
                         'type':    1}]
    compte['statuts'] = [statut_adonis]
    return compte


@fixture
def mot_de_passe_adonis_seed_0() -> str:
    # Valeur calculée par la fonction _mot_de_passe_adonis(), uniquement
    # après random.seed(0), obtenue empiriquement.
    return '~%LwSJ&APZ+Sy%2w,{_+'


@fixture
def recuperation_compte(requests_mock, compte_adonis: JSONcompte):
    compteId = compte_adonis['compteId']
    requests_mock.get(
        SERVEUR + f'/accounts/{compteId}/statuts,emails',
        status_code=200,
        json=compte_adonis,
    )


@fixture
def recherche_compte_par_identifiant(requests_mock, compte_adonis: JSONcompte):
    login = compte_adonis['identifiant']
    requests_mock.get(
        SERVEUR + f'/accounts?identifiant={login}',
        status_code=200,
        json={'compteId': compte_adonis['compteId']},
    )
