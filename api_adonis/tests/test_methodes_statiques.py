import random

from api_adonis.api_adonis import Adonis


def test_mot_de_passe_adonis(adonis: Adonis, mot_de_passe_adonis_seed_0: str):
    """Teste la génération de mot de passe à la norme Adonis."""
    random.seed(0)
    assert adonis._mot_de_passe_adonis() == mot_de_passe_adonis_seed_0


def test_comparaison_noms_adonis(adonis: Adonis):
    """Teste l'équivalence de noms vus par Adonis."""
    assert adonis._comparaison_noms_adonis('A-Bé', 'a be')
    assert not adonis._comparaison_noms_adonis('A', 'B')


def test_email_compte(adonis: Adonis, compte_adonis):
    assert (adonis.email_compte(compte_adonis)
            == 'bernard.massot@universite-paris-saclay.fr')


def test_statut_pertinent(adonis: Adonis, compte_adonis, statut_adonis):
    assert (adonis._statut_pertinent(compte_adonis, 'Personnel co-tutélaire')
            == statut_adonis)
