#!/usr/bin/python3
"""Liste des mails des comptes gitlab au login modifié dans LDAP"""
from gitlab.client import Gitlab # paquet python3-gitlab

from mots_de_passe import TOKEN_GITLAB


SERVEUR_GITLAB = 'https://gitlab.math.u-psud.fr/'

gitlab = Gitlab(SERVEUR_GITLAB, private_token=TOKEN_GITLAB)
users = gitlab.users.list(all=True)
mails = []
for user in users:
    # Seuls les utilisateurs dont le compte est associé à LDAP ont un
    # attribut identities.
    if user.identities and '.' in user.username:
        mails.append(user.email)

print(','.join(mails))
