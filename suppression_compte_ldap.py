#!/usr/bin/python3
"""Suppression d'un compte LDAP de l'IMO"""
import sys
from datetime import date
import os
from shutil import move, rmtree
from subprocess import run

from ldap3 import ALL_ATTRIBUTES

from annuaires_ldap.utilitaires_ldap import OU_COMPTES_IMO, OU_AUTO_HOME_IMO
from annuaires_ldap.annuaires import AnnuaireIMO


class LoginInexistant(Exception): pass


def suppression_compte(login: str):
    """Supprime un compte LDAP"""
    imo = AnnuaireIMO()
    if not imo.ldap.search(OU_COMPTES_IMO, f'(uid={login})',
                           attributes=ALL_ATTRIBUTES):
        raise LoginInexistant
    compte = imo.ldap.entries[0]
    imo.ldap.search(OU_AUTO_HOME_IMO, f'(cn={login})',
                    attributes=ALL_ATTRIBUTES)
    auto_home = imo.ldap.entries[0]
    aujourdhui = date.today().isoformat()

    # Supression des entrées LDAP et sauvegarde en LDIF
    with open(f'Archives-Suppressions/{aujourdhui}-{login}.ldif', 'w') as f:
        # Compte utilisateur
        f.write(compte.entry_to_ldif() + '\n')
        imo.ldap.delete(compte.entry_dn)

        # Entrée automount
        f.write(auto_home.entry_to_ldif() + '\n')
        imo.ldap.delete(auto_home.entry_dn)

    # Archivage des données utilisateur
    home = auto_home.automountInformation.value.split(':')[-1]

    wwwdoc = os.path.join(os.sep, 'userswww', login, 'wwwdoc')
    if os.path.isdir(wwwdoc):
        os.unlink(os.path.join(home, 'wwwdoc'))
        move(wwwdoc, home)
        rmtree(os.path.join(os.sep, 'userswww', login))

    # On ne garde que les 2 premiers répertoires du home à cause de
    # l'arborescence particulière d'ANEDP.
    archive = os.path.join(os.sep,
                           home.split(os.sep)[1],
                           'old-home',
                           home.split(os.sep)[2],
                           f'{login}-{aujourdhui}.tar.gz')
    run(['tar', 'zcf', archive, home])
    rmtree(home)


if __name__ == '__main__':
    if len(sys.argv) != 2:
        print('Syntaxe :', sys.argv[0], 'login')
        exit(1)
    login = sys.argv[1]
    try:
        suppression_compte(login)
        print('Utilisateur', login, 'supprimé.')
    except LoginInexistant:
        print('Login inexistant :', login)
        exit(1)
